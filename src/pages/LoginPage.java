package pages;

import mobgen.interviewlibrary.logic.AbstractTest;
import mobgen.interviewlibrary.logic.Device;
import mobgen.interviewlibrary.logic.ScreenTag;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Created by marioalvarezmiyar on 30/11/15.
 */
public class LoginPage extends AbstractTest{
    //Fields
    ScreenTag emailTextField;
    ScreenTag loginButton;

    //Data
    String userEmail;

    public LoginPage(Device device){
        super(device);
        this.device = device;
        emailTextField = new ScreenTag("Email_textf", "and_email");
        this.loginButton = new ScreenTag("login_button", "and_Login");
    }

    public void setUserEmail(String email){
        this.userEmail = email;
    }

    @Override
    public void execute() {
        try {
            this.device.getDriver().sendKeys(this.emailTextField, this.userEmail);
            this.device.getDriver().click(this.loginButton);
        } catch (MalformedURLException e) {
            try {
                this.handleException(e,"execute()");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
