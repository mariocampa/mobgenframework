package pages;

import mobgen.interviewlibrary.logic.AbstractTest;
import mobgen.interviewlibrary.logic.Device;
import mobgen.interviewlibrary.logic.ScreenTag;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Created by marioalvarezmiyar on 30/11/15.
 */
public class SearchPage extends AbstractTest{
    //Fields
    ScreenTag searchTextField;
    ScreenTag searchButton;

    //Data
    String textSearch;

    public SearchPage(Device device){
        super(device);
        this.device = device;
        searchTextField = new ScreenTag("Search_textf", "and_text");
        this.searchButton = new ScreenTag("search_button", "and_Search");
    }

    public void setUserSearch(String text){
        this.textSearch = text;
    }

    @Override
    public void execute() {
        try {
            this.device.getDriver().sendKeys(this.searchTextField, this.textSearch);
            this.device.getDriver().click(this.searchButton);
        } catch (MalformedURLException e) {
            try {
                this.handleException(e,"execute()");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
