package mockframework;

/**
 * Created by marioalvarezmiyar on 3/12/15.
 */

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class LibraryTestListener implements ITestListener {
    /*

    LibraryTestListener will implement ITestListener (TestNG) in order to print the results of each executed test

    * */
    public void onTestStart(ITestResult result) {
        System.out.println("TEST " +  getTestMethodName(result));
    }

    public void onTestSuccess(ITestResult result) {
        System.out.println("==================== " + getTestMethodName(result) + " PASSED =====================\n");
    }

    public void onTestFailure(ITestResult result) {
        System.out.println("==================== " + getTestMethodName(result) + " FAILED =====================\n");
    }


    public void onTestSkipped(ITestResult result) {
        System.out.println("==================== " + getTestMethodName(result) + " SKIPPED =====================\n");

    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }


    public void onStart(ITestContext context) {
      //  System.out.println("EXECUTING TEST CLASS: " + context.getCurrentXmlTest());
    }


    public void onFinish(ITestContext context) {
       // System.out.println("EXECUTION FINISHED: " + context.getCurrentXmlTest());
    }

    private static String getTestMethodName(ITestResult result) {
        return result.getMethod().getConstructorOrMethod().getName();
    }
}