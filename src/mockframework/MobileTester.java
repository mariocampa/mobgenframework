package mockframework;

/**
 * Created by marioalvarezmiyar on 1/12/15.
 */
public class MobileTester {

    /*

    MobileTester will provide us one instance both Managers (DevicesManager and TestManager)
    To share only instance between all the classes we will use Singleton Pattern

    * */

    private static MobileTester instance = null;
    private static DevicesManager devicesManager;
    private static TestManager testManager;

    public static MobileTester getInstance() {
        if(instance == null) {
            instance = new MobileTester();
            devicesManager = new DevicesManager();
            testManager = new TestManager();
        }
        return instance;
    }

    public static DevicesManager getDeviceManager(){
        return devicesManager;
    }

    public static TestManager getTestManager(){
        return testManager;
    }





}
