package mockframework;

import mobgen.interviewlibrary.logic.AndroidDevice;
import mobgen.interviewlibrary.logic.IOSDevice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Created by marioalvarezmiyar on 1/12/15.
 */
public class MenuFramework {
        /*

    MenuFramework class will provide us a menu with all the options of our framework in order to
    allow the user to interact with our new test center management system

    * */

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Boolean cont = true;
        while (cont) {
            showMainMenu();
            try {
                int selectedOption = Integer.parseInt(br.readLine());
                switch (selectedOption) {
                    case 1:
                        String deviceName, udId;
                        System.out.println("Type '0' for IOSDevice or '1' for AndroidDevice");
                        try {
                            int tDevice = Integer.parseInt(br.readLine());

                            if(tDevice == 1){
                                System.out.println("Insert the name of your AndroidDevice");
                                deviceName = br.readLine();
                                System.out.println("Insert the UdID of your AndroidDevice");
                                udId = br.readLine();
                                MobileTester.getInstance().getDeviceManager().addNewDevice(new AndroidDevice(deviceName,udId));
                                System.out.println("Android device added OK");
                            }
                            else if (tDevice == 0){
                                System.out.println("Insert the name of your IOSDevice");
                                deviceName = br.readLine();
                                System.out.println("Insert the UdID of your IOSDevice");
                                udId = br.readLine();
                                MobileTester.getInstance().getDeviceManager().addNewDevice(new IOSDevice(deviceName,udId));
                                System.out.println("IOS device added OK");
                            }
                        } catch (NumberFormatException e){
                            System.out.println("Error: Invalid device value");
                        }


                        break;
                    case 2:
                        System.out.println("List of Devices");
                        MobileTester.getInstance().getDeviceManager().printDevices();
                        System.out.println("Press the number of the Device you want to remove");
                        MobileTester.getInstance().getDeviceManager().removeDevice(Integer.parseInt(br.readLine()));
                        break;
                    case 3:
                        System.out.println("List of available Test Classes");
                        MobileTester.getInstance().getTestManager().printAvailableTest();
                        System.out.println("Press the number of the Test Class you want to add");
                        MobileTester.getInstance().getTestManager().addTestToLib(Integer.parseInt(br.readLine()));
                        break;
                    case 4:
                        System.out.println("List of Test Classes in Library");
                        MobileTester.getInstance().getTestManager().printLibraryTest();
                        System.out.println("Press the number of the Test Class you want to remove from Library");
                        MobileTester.getInstance().getTestManager().removeTestFromLib(Integer.parseInt(br.readLine()));
                        break;
                    case 5:
                        MobileTester.getInstance().getTestManager().executeTestLibrary();
                        break;
                    case 6:
                        MobileTester.getInstance().getDeviceManager().printDevices();
                        break;
                    case 7:
                        System.out.println("List of Test Classes in Library");
                        MobileTester.getInstance().getTestManager().printLibraryTest();
                        break;
                    case 8:
                        cont = false;
                        System.out.println("BYE!!!");
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }catch (NumberFormatException e){
                System.out.println("Error: Invalid option");
            }
        }
    }

    public static void showMainMenu(){
        // Display menu graphics
        System.out.println("====================================");
        System.out.println("|   MENU FRAMEWORK ACTION           |");
        System.out.println("====================================");
        System.out.println("| Options:                          |");
        System.out.println("|        1. Add Device              |");
        System.out.println("|        2. Remove Device           |");
        System.out.println("|        3. Add Test Class          |");
        System.out.println("|        4. Remove Test Class       |");
        System.out.println("|        5. Execute Test            |");
        System.out.println("|        6. Show Devices            |");
        System.out.println("|        7. Show Test Library       |");
        System.out.println("|        8. Exit                    |");
        System.out.println("====================================");
    }

}

