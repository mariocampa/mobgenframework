package mockframework;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by marioalvarezmiyar on 1/12/15.
 */
public class TestManager {

    /*

    TestManager class will manage our Devices it will provide next actions
    - add new test class to the library
    - remove test class from library
    - print the list of defined test classes in testlibrarypackage
    - print the list of test classes added into our library
    - clear the manager
    - execute test in the library using TestNG executor (xml built from code)

    IMPORTANT: All the test test classes will be placed in testlibrary package. They will be defined using TestNG annotations

    * */


    private ArrayList<XmlClass> libraryTest;
    private ArrayList<XmlClass> availableTest;

    public TestManager (){
        libraryTest = new ArrayList<XmlClass>();
        availableTest = new ArrayList<XmlClass>();
        getTestFromFolder();
    }

    public void addTestToLib(int pos){
        if (pos >availableTest.size()) return;
        if (availableTest.size() == 0){
            System.out.println("No Test Class Defined");
            return;
        }
        libraryTest.add(availableTest.get(pos-1));
        availableTest.remove(pos-1);
        System.out.println(libraryTest.get(libraryTest.size()-1).getName() + " ------> LIB OK");
    }

    public void removeTestFromLib(int pos){
        if (pos >libraryTest.size()) return;
        if (libraryTest.size() == 0){
            System.out.println("No Test Class in Library");
            return;
        }
        availableTest.add(libraryTest.get(pos-1));
        libraryTest.remove(pos-1);
        System.out.println(availableTest.get(availableTest.size()-1).getName() + " <------ OUT LIB OK");

    }

    public void resetLibrary(){
        libraryTest.clear();
        getTestFromFolder();
    }


    public void printAvailableTest(){
        if (availableTest.size() == 0){
            System.out.println("No test defined");
            return;
        }
        for (int i=0; i<availableTest.size();i++){
            System.out.println((i+1) + ". " + availableTest.get(i).getName());
        }
    }

    public void printLibraryTest(){
        if (libraryTest.size() == 0){
            System.out.println("No test added in library");
            return;
        }
        for (int i=0; i<libraryTest.size();i++){
            System.out.println((i+1) + ". " + libraryTest.get(i).getName());
        }
    }

    public void executeTestLibrary(){
        if (libraryTest.size()>0){
            TestNG myTestNG = new TestNG();

            //Create an instance of XML Suite and assign a name for it.
            XmlSuite mySuite = new XmlSuite();
            mySuite.setName("Library Test");

            //Create an instance of XmlTest and assign a name for it.
            XmlTest myTest = new XmlTest(mySuite);
            myTest.setName("Added Test");

            //Create a list which can contain the classes that you want to run.
            List<XmlClass> myClasses = new ArrayList<XmlClass>();
            for (int i = 0; i<libraryTest.size(); i++){
                myClasses.add(libraryTest.get(i));
            }


            //Assign that to the XmlTest Object created earlier.
            myTest.setXmlClasses(myClasses);

            //Create a list of XmlTests and add the Xmltest you created earlier to it.
            List<XmlTest> myTests = new ArrayList<XmlTest>();
            myTests.add(myTest);

            //add the list of tests to your Suite.
            mySuite.setTests(myTests);

            //Add the suite to the list of suites.
            List<XmlSuite> mySuites = new ArrayList<XmlSuite>();
            mySuites.add(mySuite);

            //Set the list of Suites to the testNG object you created earlier.
            myTestNG.setXmlSuites(mySuites);
            List<Class> listnerClasses = new ArrayList<Class>();
            listnerClasses.add(LibraryTestListener.class);
            myTestNG.setListenerClasses(listnerClasses);

            //invoke run() - this will run your class.
            myTestNG.run();
        }

    }

    private void getTestFromFolder(){
        String currentDir = System.getProperty("user.dir");
        String libraryDir = currentDir + "/src/testlibrary/";
        //System.out.println("Test dir:" + libraryDir);
        listf(libraryDir);
    }

    private void listf(String path) {
        File directory = new File(path);
        // get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isFile()) {
                try {
                    if(!file.getName().startsWith("."))//Not hidden files
                        availableTest.add(new XmlClass(file.getCanonicalPath().substring(file.getCanonicalPath().indexOf("testlibrary/"),file.getCanonicalPath().lastIndexOf(".")).replace("/",".")));
                } catch (IOException e) {
                    System.out.println("Error: File not found");
                }
            } else if (file.isDirectory()) {
                listf(file.getAbsolutePath());
            }
        }
    }


}
