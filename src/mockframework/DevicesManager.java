package mockframework;

import mobgen.interviewlibrary.logic.AndroidDevice;
import mobgen.interviewlibrary.logic.Device;

import java.util.ArrayList;

/**
 * Created by marioalvarezmiyar on 1/12/15.
 */
public class DevicesManager {

    /*

    DevicesManager class will manage our Devices it will provide next actions
    - add new device
    - remove existing device
    - print the list of available devices
    - clear the manager
    - getter of devices (ArrayList)

    * */
    private ArrayList<Device> devices = null;


    public DevicesManager (){
        devices = new ArrayList<Device>();
    }

    public void addNewDevice(Device device){
        devices.add(device);
        System.out.println(device.getDeviceName() + " "+ device.getUdid() + "------> ADDED");
    }

    public void removeDevice(int pos){
        if (devices.size() == 0){
            System.out.println("No Available devices");
            return;
        }
        if (pos >devices.size()) return;
        Device removed = devices.remove(pos-1);
        System.out.println(removed.getDeviceName() + " "+ removed.getUdid() + " <------ REMOVED");
    }

    public void clearManager(){
        devices.clear();
    }

    public void printDevices(){

        for (int i=0; i<devices.size();i++){
            String disp;
            if(devices.get(i) instanceof AndroidDevice) disp = "Android Device: ";
            else disp = "IOS Device: ";
            System.out.println((i+1)+". " + disp + devices.get(i).getDeviceName() + " UdId: "+devices.get(i).getUdid());
        }
    }

    public ArrayList<Device> getDevices() {
        return devices;
    }
}
