package testlibrary.searchtest;

import mobgen.interviewlibrary.logic.Device;
import mockframework.MobileTester;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.SearchPage;

import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 * Created by marioalvarezmiyar on 30/11/15.
 */
public class SearchPageTestA {
    Device device;
    SearchPage searchPage;

    //DataProvider annotation allows us to execute all defined test with all added devices
    @DataProvider(name="devicesProvider")
    public Object[][] getDevice() {
        ArrayList<Device> testDevices = MobileTester.getInstance().getDeviceManager().getDevices();

        Object [][] objArray = new Object[testDevices.size()][];

        for(int i=0;i< testDevices.size();i++){
            objArray[i] = new Object[1];
            objArray[i][0] = testDevices.get(i);
        }
        return objArray;
    }

    @BeforeMethod
    public void setUp(){
        System.out.println("\nBefore method actions");
    }


    @Test(dataProvider= "devicesProvider")
    public void searchPageTestA(Device device){
        //This code should be on setUp, nonetheless due to the DataProvider we will place it here
        this.device = device;
        searchPage = new SearchPage(device);
        searchPage.setUserSearch("this is an interview");
        searchPage.execute();
        //IMPORTANT: We woukd need to check with an assert if the action is executing correctly, InterviewDriver should provide these actions
    }

    @Test(dataProvider= "devicesProvider")
    public void searchPageTestB(Device device){
        //This code should be on setUp, nonetheless due to the DataProvider we will place it here
        this.device = device;
        searchPage = new SearchPage(device);
        searchPage.setUserSearch("this is another interview");
        searchPage.execute();
        //IMPORTANT: We woukd need to check with an assert if the action is executing correctly, InterviewDriver should provide these actions
    }

    @Test(dataProvider= "devicesProvider")
    public void searchPageTestC(Device device){
        //This code should be on setUp, nonetheless due to the DataProvider we will place it here
        this.device = device;
        searchPage = new SearchPage(device);
        searchPage.setUserSearch("not found interview");
        searchPage.execute();
        //IMPORTANT: We woukd need to check with an assert if the action is executing correctly, InterviewDriver should provide these actions
        //Test will fail
        Assert.fail();
    }

    @AfterMethod
    public void tearDown(){
        try {
            device.resetApp();
            device.releaseDriver();
        } catch (MalformedURLException e) {
        }
        device = null;
        searchPage = null;
    }
}
