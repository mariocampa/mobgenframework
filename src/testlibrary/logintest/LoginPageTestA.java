package testlibrary.logintest;

import mobgen.interviewlibrary.logic.Device;

import org.testng.annotations.*;


import mockframework.MobileTester;
import java.net.MalformedURLException;
import pages.LoginPage;
import java.util.ArrayList;

/**
 * Created by marioalvarezmiyar on 30/11/15.
 */
public class LoginPageTestA {
    Device device;
    LoginPage loginPage;

    //DataProvider annotation allows us to execute all defined test with all added devices
    @DataProvider(name="devicesProvider")
    public Object[][] getDevice() {
        ArrayList<Device> testDevices = MobileTester.getInstance().getDeviceManager().getDevices();

        Object [][] objArray = new Object[testDevices.size()][];

        for(int i=0;i< testDevices.size();i++){
            objArray[i] = new Object[1];
            objArray[i][0] = testDevices.get(i);
        }
        return objArray;
    }

    @BeforeMethod
    public void setUp(){
        System.out.println("\nBefore method actions");
    }


    @Test(dataProvider= "devicesProvider")
    public void loginPageTestA(Device device){
        this.device = device;
        loginPage = new LoginPage(device);
        loginPage.setUserEmail("interviewA@mobgen.com");
        loginPage.execute();
        //IMPORTANT: We woukd need to check with an assert if the action is executing correctly, InterviewDriver should provide these actions
    }

    @AfterMethod
    public void tearDown(){
        try {
            device.resetApp();
            device.releaseDriver();
        } catch (MalformedURLException e) {
        }
        device = null;
        loginPage = null;
    }
}
