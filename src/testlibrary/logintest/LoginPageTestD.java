package testlibrary.logintest;

import mobgen.interviewlibrary.logic.Device;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import mockframework.MobileTester;
import java.net.MalformedURLException;
import pages.LoginPage;
import java.util.ArrayList;

/**
 * Created by marioalvarezmiyar on 30/11/15.
 */
public class LoginPageTestD {
    Device device;
    LoginPage loginPage;

    //DataProvider annotation allows us to execute all defined test with all added devices
    @DataProvider(name="devicesProvider")
    public Object[][] getDevice() {
        ArrayList<Device> testDevices = MobileTester.getInstance().getDeviceManager().getDevices();

        Object [][] objArray = new Object[testDevices.size()][];

        for(int i=0;i< testDevices.size();i++){
            objArray[i] = new Object[1];
            objArray[i][0] = testDevices.get(i);
        }
        return objArray;
    }

    @BeforeMethod
    public void setUp(){
        System.out.println("\nBefore method actions");
    }


    @Test(dataProvider= "devicesProvider")
    public void loginPageTestD(Device device){
        //This code should be on setUp, nonetheless due to the DataProvider we will place it here
        this.device = device;
        loginPage = new LoginPage(device);
        loginPage.setUserEmail("interviewDfailed@mobgen.com");
        loginPage.execute();
        //IMPORTANT: We woukd need to check with an assert if the action is executing correctly, InterviewDriver should provide these actions
        //Test will fail
        Assert.fail();
    }

    @AfterMethod
    public void tearDown(){
        try {
            device.resetApp();
            device.releaseDriver();
        } catch (MalformedURLException e) {
        }
        device = null;
        loginPage = null;
    }
}
